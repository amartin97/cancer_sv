# cancer_sv

## Introduction

cancer_sv is a repository that offers a [snakemake](https://snakemake.readthedocs.io/en/stable/) workflow in order to develop a structural variation (SV) analysis on cancer sequencing data.

WARNING!: workflow creation in progress.

## Dependencies

- [NanoPlot](https://github.com/wdecoster/NanoPlot)
- [SAMtools](https://github.com/samtools/samtools)
- [pigz](https://zlib.net/pigz/)
- [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
- [BWA](https://github.com/lh3/bwa)
- [Minimap2](https://github.com/lh3/minimap2)
- [Qualimap2](http://qualimap.conesalab.org/)
- [Sniffles](https://github.com/fritzsedlazeck/Sniffles)
- [Sambamba](https://github.com/biod/sambamba)
- [Manta](https://github.com/Illumina/manta)
- [Strelka2](https://github.com/Illumina/strelka)

All of these dependencies can be managed by using the conda environment files allocated in the `envs` directory and by adding the `--use-conda` argument to the snakemake execution command.

## Recommended execution (HPC-based)

`snakemake --use-conda --cores <NUM> -p --profile <cluster_profile> <command>`
* NUM: desired number of cores
* cluster_profile: it depends on the cluster used (See available files at https://github.com/snakemake-profiles).
* command: name of the desired target rule of the Snakefile.
    * `initial_metrics`: it executes `NanoStat` over the ONT fastq input files and `FastQC` over Illumina fastq files. In the case of this project, as the Illumina input files are bam files, the paired-end fastq files (`samtools` + `pigz`) have to be created.
    * `GSC_metrics`: it sorts the Illumina bam input files (`samtools`) and executes `qualimap2` over the resulting sorted bam files.
    * `alignments_illumina`: it aligns the created fastq.gz files against the indexed reference genome (indicated in config.yaml) with `minimap2` and/or `bwa`; sorts the resulting alignment (`samtools`); and executes `qualimap2` over a coordinate sorted bam file.
    * `illumina_sv_calling`: it removes duplicates from the Illumina sorted bam files (`sambamba`); calls for somatic indel candidates (`manta`); and calls for somatic SNVs and indels (`strelka`), by using also the vcf file with the previously identified indel candidates.
    * `alignment_nanopore`: it aligns ONT reads against the indexed reference genome (`minimap2`); compresses and sorts the resulting sam file (`samtools`); and executes `qualimap2` over the resulting sorted bam file.
    * `nanopore_sv_calling`: it executes `sniffles` over the ONT sorted bam files.

## Author

[Alejandro Martín Muñoz](https://gitlab.com/amartin97)

## Contributors

[Tomás Di Domenico](https://gitlab.com/tdido)
