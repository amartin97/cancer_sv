# ONT: Metrics calculation
rulename = "nanopore_metrics_calc"
rule nanopore_metrics_calc:
    input:
        lambda wildcards: config["samples"]["nanopore"][wildcards.sample]
    output:
        directory(f"{outdir}/nanopore/metrics_res/nanopore_{{sample}}/")
    conda:
        "../envs/nanoplot.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/nanopore/metrics_res/nanopore_{{sample}}/execution/nanoplot_{{sample}}.out",
        err = f"{logdir}/nanopore/metrics_res/nanopore_{{sample}}/execution/nanoplot_{{sample}}.err"
    benchmark:
        f"{logdir}/nanopore/metrics_res/nanopore_{{sample}}/execution/nanoplot_{{sample}}.bmk"
    shell:
        "NanoPlot -t {threads} --verbose --fastq {input} -o {output} > {log.out} 2> {log.err}"

# ONT: Subsampling and alignment (initial test)
rulename = "nanopore_subsampling"
rule nanopore_subsampling:
    input:
        lambda wildcards: config["samples"]["nanopore"][wildcards.sample]
    output:
        f"{outdir}/nanopore/subsampling/nanopore_{{sample}}_10000.fastq"
    conda:
        "../envs/seqtk.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        f"{logdir}/nanopore/subsampling/execution/nanopore_{{sample}}_10000.fastq.log"
    benchmark:
        f"{logdir}/nanopore/subsampling/execution/nanopore_{{sample}}_10000.fastq.bmk"
    shell:
        "seqtk sample {input} 10000 > {output} 2> {log}"

rulename = "nanopore_subsampling_compression"
rule nanopore_subsampling_compression:
    input:
        f"{outdir}/nanopore/subsampling/nanopore_{{sample}}.fastq"
    output:
        f"{outdir}/nanopore/subsampling/nanopore_{{sample}}.fastq.gz"
    conda:
        "../envs/pigz.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/nanopore/subsampling/execution/nanopore_{{sample}}.fastq.gz.out",
        err = f"{logdir}/nanopore/subsampling/execution/nanopore_{{sample}}.fastq.gz.err"
    benchmark:
        f"{logdir}/nanopore/subsampling/execution/nanopore_{{sample}}.fastq.gz.bmk"
    shell:
        "pigz -9 -p {threads} {input} > {log.out} 2> {log.err}"

rulename = "nanopore_subsampling_alignment"
rule nanopore_subsampling_alignment:
    input:
        f"{outdir}/genome/minimap2/reference_genome_{{version}}.mmi",
        f"{outdir}/nanopore/subsampling/nanopore_{{sample}}.fastq.gz"
    output:
        f"{outdir}/nanopore/sorted_bam/nanopore_{{sample}}_{{version}}_sorted.bam"
    conda:
        "../envs/minimap2.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/nanopore/sam/execution/nanopore_{{sample}}_{{version}}_sorted.bam.out",
        err = f"{logdir}/nanopore/sam/execution/nanopore_{{sample}}_{{version}}_sorted.bam.err"
    benchmark:
        f"{logdir}/nanopore/sam/execution/nanopore_{{sample}}_{{version}}_sorted.bam.bmk"
    shell:
        """
        minimap2 --MD -ax map-ont -R "@RG\\tID:{wildcards.sample}\\tSM:{wildcards.sample}" -t {threads} {input} | \
        samtools sort -l 9 --threads {threads} -m 2G -o {output} - > {log.out} 2> {log.err}
        """

# ONT: Alignment
rulename = "nanopore_alignment"
rule nanopore_alignment:
    input:
        i_ref = f"{outdir}/genome/minimap2/reference_genome_{{version}}.mmi",
        sm = lambda wildcards: config["samples"]["nanopore"][wildcards.sample]
    output:
        f"{outdir}/nanopore/sorted_bam/nanopore_{{sample}}_{{version}}_sorted.bam"
    conda:
        "../envs/minimap2.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/nanopore/sam/execution/nanopore_{{sample}}_{{version}}_sorted.bam.out",
        err = f"{logdir}/nanopore/sam/execution/nanopore_{{sample}}_{{version}}_sorted.bam.err"
    benchmark:
        f"{logdir}/nanopore/sam/execution/nanopore_{{sample}}_{{version}}_sorted.bam.bmk"
    shell:
        """
        minimap2 --MD -ax map-ont -R "@RG\\tID:{wildcards.sample}\\tSM:{wildcards.sample}" -t {threads} {input.i_ref} {input.sm} | \
        samtools sort -l 9 --threads {threads} -m 2G -o {output} - > {log.out} 2> {log.err}
        """

# Alignment metrics calculation
rulename = "nanopore_alignment_metrics"
rule nanopore_alignment_metrics:
    input:
        f"{outdir}/nanopore/sorted_bam/nanopore_{{sample}}_{{version}}_sorted.bam"
    output:
        f"{outdir}/nanopore/qualimap_res/nanopore_{{sample}}_{{version}}/genome_results.txt"
    conda:
        "../envs/qualimap.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/nanopore/qualimap_res/execution/nanopore_{{sample}}_{{version}}.out",
        err = f"{logdir}/nanopore/qualimap_res/execution/nanopore_{{sample}}_{{version}}.err"
    benchmark:
        f"{logdir}/nanopore/qualimap_res/execution/nanopore_{{sample}}_{{version}}.bmk"
    shell:
        "qualimap bamqc -bam {input} -outdir $(dirname {output}) -nt {threads} -c --java-mem-size={resources.mem_mb}M -nw 1000 -nr 500 "
        "> {log.out} 2> {log.err}"

# ONT: SV calling
rulename = "sniffles_sv_calling"
rule sniffles_sv_calling:
    input:
        f"{outdir}/nanopore/sorted_bam/nanopore_{{sample}}_{{version}}_sorted.bam"
    output:
        f"{outdir}/nanopore/sniffles_res/nanopore_{{sample}}_{{version}}_SV_calling.vcf"
    conda:
        "../envs/sniffles.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/nanopore/sniffles_res/execution/nanopore_{{sample}}_{{version}}_SV_calling.vcf.out",
        err = f"{logdir}/nanopore/sniffles_res/execution/nanopore_{{sample}}_{{version}}_SV_calling.vcf.err"
    benchmark:
        f"{logdir}/nanopore/sniffles_res/execution/nanopore_{{sample}}_{{version}}_SV_calling.vcf.bmk"
    shell:
        "sniffles -m {input} -v {output} --threads {threads} > {log.out} 2> {log.err}"
