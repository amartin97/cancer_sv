# Illumina: bam sorting
rulename = "illumina_bam_sorting"
rule illumina_bam_sorting:
    input:
        lambda wildcards: config["samples"]["illumina"][wildcards.sample]
    output:
        f"{outdir}/illumina/sorted_bam/GSC_{{sample}}_37_sorted.bam"
    conda:
        "../envs/samtools.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/illumina/sorted_bam/execution/GSC_{{sample}}_37_sorted.bam.out",
        err = f"{logdir}/illumina/sorted_bam/execution/GSC_{{sample}}_37_sorted.bam.err"
    benchmark:
        f"{logdir}/illumina/sorted_bam/execution/GSC_{{sample}}_37_sorted.bam.bmk"
    shell:
        "samtools sort -n -l 9 --threads {threads} -m 2G {input} -o {output} > {log.out} 2> {log.err}"

# Illumina: input metrics calculation
rulename = "GSC_alignment_metrics"
rule GSC_alignment_metrics:
    input:
        f"{outdir}/illumina/sorted_bam/GSC_{{sample}}_{{version}}_sorted.bam"
    output:
        tmp = temp(f"{outdir}/illumina/sorted_bam/GSC_{{sample}}_{{version}}_sorted_tmp.bam"),
        res = f"{outdir}/illumina/qualimap_res/GSC_{{sample}}_{{version}}/genome_results.txt"
    conda:
        "../envs/qualimap.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/illumina/qualimap_res/execution/GSC_{{sample}}_{{version}}.out",
        err = f"{logdir}/illumina/qualimap_res/execution/GSC_{{sample}}_{{version}}.err"
    benchmark:
        f"{logdir}/illumina/qualimap_res/execution/GSC_{{sample}}_{{version}}.bmk"
    shell:
        """
        samtools sort -l 9 --threads {threads} -m 2G {input} -o {output.tmp} > {log.out} 2> {log.err}
        qualimap bamqc -bam {output.tmp} -outdir $(dirname {output.res}) -nt {threads} -c --java-mem-size={resources.mem_mb}M -nw 1000 -nr 500 \
                 >> {log.out} 2>> {log.err}
        """

# Illumina: creation of a fastq.gz file from a bam file
rulename = "illumina_fastq_creation"
rule illumina_fastq_creation:
    input:
        f"{outdir}/illumina/sorted_bam/GSC_{{sample}}_37_sorted.bam"
    output:
        F = f"{outdir}/illumina/reads/illumina_{{sample}}_F.fastq",
        R = f"{outdir}/illumina/reads/illumina_{{sample}}_R.fastq"
    conda:
        "../envs/samtools.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/illumina/reads/execution/illumina_{{sample}}_pair.fastq.out",
        err = f"{logdir}/illumina/reads/execution/illumina_{{sample}}_pair.fastq.err"
    benchmark:
        f"{logdir}/illumina/reads/execution/illumina_{{sample}}_pair.fastq.bmk"
    shell:
        "samtools fastq -N --threads {threads} -1 {output.F} -2 {output.R} {input} > {log.out} 2> {log.err}"

rulename = "illumina_fastq_compression"
rule illumina_fastq_compression:
    input:
        f"{outdir}/illumina/reads/illumina_{{sample}}.fastq"
    output:
        f"{outdir}/illumina/reads/illumina_{{sample}}.fastq.gz"
    conda:
        "../envs/pigz.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/illumina/reads/execution/illumina_{{sample}}.fastq.gz.out",
        err = f"{logdir}/illumina/reads/execution/illumina_{{sample}}.fastq.gz.err"
    benchmark:
        f"{logdir}/illumina/reads/execution/illumina_{{sample}}.fastq.gz.bmk"
    shell:
        "pigz -9 -p {threads} {input} > {log.out} 2> {log.err}"

# Illumina: fastq metrics calculation
rulename = "illumina_metrics_calc"
rule illumina_metrics_calc:
    input:
        f"{outdir}/illumina/reads/illumina_{{sample}}.fastq.gz"
    output:
        html = f"{outdir}/illumina/metrics_res/illumina_{{sample}}.html",
        zip = f"{outdir}/illumina/metrics_res/illumina_{{sample}}_fastqc.zip"
    params:
        f"--noextract -o {outdir}/illumina/metrics_res/"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        f"{logdir}/illumina/metrics_res/execution/illumina_{{sample}}.log"
    benchmark:
        f"{logdir}/illumina/metrics_res/execution/illumina_{{sample}}.bmk"
    wrapper:
        "0.67.0/bio/fastqc"

# Illumina: alignment
rulename = "bwa_index_reference"
rule bwa_index_reference:
    input:
        lambda wildcards: config["genome"][wildcards.version]
    output:
        fasta = f"{outdir}/genome/bwa/{{version}}.fna",
        index = f"{outdir}/genome/bwa/{{version}}.fna.bwt"
    conda:
        "../envs/bwa.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        f"{logdir}/genome/bwa/execution/reference_genome_{{version}}.bwa.log",
    benchmark:
        f"{logdir}/genome/bwa/execution/reference_genome_{{version}}.bwa.bmk"
    shell:
        """
        mkdir -p {outdir}/genome/bwa
        gunzip -c {input} > {output.fasta} 2> {log}
        bwa index -a bwtsw {output.fasta} 2>> {log}
        """

rulename = "illumina_alignment_bwa"
rule illumina_alignment_bwa:
    input:
        f"{outdir}/genome/bwa/reference_genome_{{version}}.fna",
        f"{outdir}/illumina/reads/illumina_{{sample}}_F.fastq.gz",
        f"{outdir}/illumina/reads/illumina_{{sample}}_R.fastq.gz"
    output:
        f"{outdir}/illumina/sorted_bam/illumina_{{sample}}_{{version}}_bwa_sorted.bam"
    conda:
        "../envs/bwa.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/illumina/sorted_bam/execution/illumina_{{sample}}_{{version}}_bwa_sorted.bam.out",
        err = f"{logdir}/illumina/sorted_bam/execution/illumina_{{sample}}_{{version}}_bwa_sorted.bam.err"
    benchmark:
        f"{logdir}/illumina/sorted_bam/execution/illumina_{{sample}}_{{version}}_bwa_sorted.bam.bmk"
    shell:
        """
        bwa mem -CM -R "@RG\\tID:{wildcards.sample}\\tSM:{wildcards.sample}" -t {threads} {input} | \
        samtools sort -n -l 9 --threads {threads} -m 2G -o {output} - > {log.out} 2> {log.err}
        """

rulename = "illumina_alignment_minimap2"
rule illumina_alignment_minimap2:
    input:
        f"{outdir}/genome/minimap2/reference_genome_{{version}}.mmi",
        f"{outdir}/illumina/reads/illumina_{{sample}}_F.fastq.gz",
        f"{outdir}/illumina/reads/illumina_{{sample}}_R.fastq.gz"
    output:
        f"{outdir}/illumina/sorted_bam/illumina_{{sample}}_{{version}}_minimap2_sorted.bam"
    conda:
        "../envs/minimap2.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/illumina/sorted_bam/execution/illumina_{{sample}}_{{version}}_minimap2_sorted.bam.out",
        err = f"{logdir}/illumina/sorted_bam/execution/illumina_{{sample}}_{{version}}_minimap2_sorted.bam.err"
    benchmark:
        f"{logdir}/illumina/sorted_bam/execution/illumina_{{sample}}_{{version}}_minimap2_sorted.bam.bmk"
    shell:
        """
        minimap2 --MD -ax sr -R "@RG\\tID:{wildcards.sample}\\tSM:{wildcards.sample}" -t {threads} {input} | \
        samtools sort -n -l 9 --threads {threads} -m 2G -o {output} - > {log.out} 2> {log.err}
        """

# Illumina: alignment metrics calculation
rulename = "illumina_alignment_metrics"
rule illumina_alignment_metrics:
    input:
        f"{outdir}/illumina/sorted_bam/illumina_{{sample}}_{{version}}_{{aligner}}_sorted.bam"
    output:
        tmp = temp(f"{outdir}/illumina/sorted_bam/illumina_{{sample}}_{{version}}_{{aligner}}_sorted_tmp.bam"),
        res = f"{outdir}/illumina/qualimap_res/illumina_{{sample}}_{{version}}_{{aligner}}/genome_results.txt"
    conda:
        "../envs/qualimap.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/illumina/qualimap_res/execution/illumina_{{sample}}_{{version}}_{{aligner}}.out",
        err = f"{logdir}/illumina/qualimap_res/execution/illumina_{{sample}}_{{version}}_{{aligner}}.err"
    benchmark:
        f"{logdir}/illumina/qualimap_res/execution/illumina_{{sample}}_{{version}}_{{aligner}}.bmk"
    shell:
        """
        samtools sort -l 9 --threads {threads} -m 2G {input} -o {output.tmp} > {log.out} 2> {log.err}
        qualimap bamqc -bam {output.tmp} -outdir $(dirname {output.res}) -nt {threads} -c --java-mem-size={resources.mem_mb}M -nw 1000 -nr 500 \
                 >> {log.out} 2>> {log.err}
        """

# Illumina: removing duplicates
rulename = "illumina_rmduplicates"
rule illumina_rmduplicates:
    input:
        f"{outdir}/illumina/sorted_bam/illumina_{{sample}}_{{version}}_{{aligner}}_sorted.bam"
    output:
        bam_0 = temp(f"{outdir}/illumina/rmdup_bam/illumina_{{sample}}_{{version}}_{{aligner}}_sorted_marked.bam"),
        bam_f = f"{outdir}/illumina/rmdup_bam/illumina_{{sample}}_{{version}}_{{aligner}}_csorted_marked.bam"
    conda:
        "../envs/sambamba.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/illumina/rmdup_bam/execution/illumina_{{sample}}_{{version}}_{{aligner}}_csorted_marked.bam.out",
        err = f"{logdir}/illumina/rmdup_bam/execution/illumina_{{sample}}_{{version}}_{{aligner}}_csorted_marked.bam.err"
    benchmark:
        f"{logdir}/illumina/rmdup_bam/execution/illumina_{{sample}}_{{version}}_{{aligner}}_csorted_marked.bam.bmk"
    shell:
        """
        sambamba markdup -r -t {threads} -l 9 -p {input} {output.bam_0} > {log.out} 2> {log.err}
        samtools sort -l 9 --threads {threads} -m 2G {output.bam_0} -o {output.bam_f} >> {log.out} 2>> {log.err}
        """

# Illumina: indexing of sv_calling input files
rulename = "illumina_fasta_indexing"
rule illumina_fasta_indexing:
    input:
        f"{outdir}/genome/bwa/reference_genome_{{version}}.fna"
    output:
        f"{outdir}/genome/bwa/reference_genome_{{version}}.fna.fai"
    conda:
        "../envs/samtools.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/illumina/sv_calling/execution/reference_genome_{{version}}.fna.fai.out",
        err = f"{logdir}/illumina/sv_calling/execution/reference_genome_{{version}}.fna.fai.err"
    benchmark:
        f"{logdir}/illumina/sv_calling/execution/reference_genome_{{version}}.fna.fai.bmk"
    shell:
        "samtools faidx {input} -o {output} > {log.out} 2> {log.err}"

rulename = "illumina_bam_indexing"
rule illumina_bam_indexing:
    input:
        f"{outdir}/illumina/rmdup_bam/illumina_{{sample}}_{{version}}_{{aligner}}_csorted_marked.bam"
    output:
        f"{outdir}/illumina/rmdup_bam/illumina_{{sample}}_{{version}}_{{aligner}}_csorted_marked.bam.bai"
    conda:
        "../envs/samtools.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/illumina/sv_calling/execution/illumina_{{sample}}_{{version}}_{{aligner}}_csorted_marked.bam.bai.out",
        err = f"{logdir}/illumina/sv_calling/execution/illumina_{{sample}}_{{version}}_{{aligner}}_csorted_marked.bam.bai.err"
    benchmark:
        f"{logdir}/illumina/sv_calling/execution/illumina_{{sample}}_{{version}}_{{aligner}}_csorted_marked.bam.bai.bmk"
    shell:
        "samtools index -b -@ {threads} {input} > {log.out} 2> {log.err}"

# Illumina: Manta sv calling
rulename = "illumina_manta_calling"
rule illumina_manta_calling:
    input:
        f"{outdir}/genome/bwa/reference_genome_{{version}}.fna.fai",
        f"{outdir}/illumina/rmdup_bam/illumina_" + config["condition"]["illumina"]["normal"] + f"_{{version}}_{{aligner}}_csorted_marked.bam.bai",
        f"{outdir}/illumina/rmdup_bam/illumina_" + config["condition"]["illumina"]["tumor"] + f"_{{version}}_{{aligner}}_csorted_marked.bam.bai",
        ref = f"{outdir}/genome/bwa/reference_genome_{{version}}.fna",
        normal = f"{outdir}/illumina/rmdup_bam/illumina_" + config["condition"]["illumina"]["normal"] + f"_{{version}}_{{aligner}}_csorted_marked.bam",
        tumor = f"{outdir}/illumina/rmdup_bam/illumina_" + config["condition"]["illumina"]["tumor"] + f"_{{version}}_{{aligner}}_csorted_marked.bam"
    output:
        f"{outdir}/illumina/sv_calling/manta/GRCh{{version}}_{{aligner}}/results/variants/candidateSV.vcf.gz"
    conda:
        "../envs/manta.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/illumina/sv_calling/execution/manta_GRCh{{version}}_{{aligner}}.out",
        err = f"{logdir}/illumina/sv_calling/execution/manta_GRCh{{version}}_{{aligner}}.err"
    benchmark:
        f"{logdir}/illumina/sv_calling/execution/manta_GRCh{{version}}_{{aligner}}.bmk"
    shell:
        """
        configManta.py --normalBam {input.normal} \
                       --tumorBam {input.tumor} \
                       --referenceFasta {input.ref} \
                       --runDir {outdir}/illumina/sv_calling/manta/GRCh{wildcards.version}_{wildcards.aligner} \
                       > {log.out} 2> {log.err}
        {outdir}/illumina/sv_calling/manta/GRCh{wildcards.version}_{wildcards.aligner}/runWorkflow.py -j {threads} >> {log.out} 2>> {log.err}
        """

# Illumina: Strelka sv calling
rulename = "illumina_strelka_calling"
rule illumina_strelka_calling:
    input:
        f"{outdir}/genome/bwa/reference_genome_{{version}}.fna.fai",
        f"{outdir}/illumina/rmdup_bam/illumina_" + config["condition"]["illumina"]["normal"] + f"_{{version}}_{{aligner}}_csorted_marked.bam.bai",
        f"{outdir}/illumina/rmdup_bam/illumina_" + config["condition"]["illumina"]["tumor"] + f"_{{version}}_{{aligner}}_csorted_marked.bam.bai",
        ref = f"{outdir}/genome/bwa/reference_genome_{{version}}.fna",
        normal = f"{outdir}/illumina/rmdup_bam/illumina_" + config["condition"]["illumina"]["normal"] + f"_{{version}}_{{aligner}}_csorted_marked.bam",
        tumor = f"{outdir}/illumina/rmdup_bam/illumina_" + config["condition"]["illumina"]["tumor"] + f"_{{version}}_{{aligner}}_csorted_marked.bam",
        candidates = f"{outdir}/illumina/sv_calling/manta/GRCh{{version}}_{{aligner}}/results/variants/candidateSV.vcf.gz"
    output:
        f"{outdir}/illumina/sv_calling/strelka/GRCh{{version}}_{{aligner}}/results/variants/somatic.indels.vcf.gz"
    conda:
        "../envs/strelka.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/illumina/sv_calling/execution/strelka_GRCh{{version}}_{{aligner}}.out",
        err = f"{logdir}/illumina/sv_calling/execution/strelka_GRCh{{version}}_{{aligner}}.err"
    benchmark:
        f"{logdir}/illumina/sv_calling/execution/strelka_GRCh{{version}}_{{aligner}}.bmk"
    shell:
        """
        configureStrelkaSomaticWorkflow.py --normalBam {input.normal} \
                                           --tumorBam {input.tumor} \
                                           --referenceFasta {input.ref} \
                                           --indelCandidates {input.candidates} \
                                           --runDir {outdir}/illumina/sv_calling/strelka/GRCh{wildcards.version}_{wildcards.aligner} \
                                           > {log.out} 2> {log.err}
        {outdir}/illumina/sv_calling/strelka/GRCh{wildcards.version}_{wildcards.aligner}/runWorkflow.py -m local \
                                                                                                        -j {threads} \
                                                                                                        >> {log.out} 2>> {log.err}
        """
