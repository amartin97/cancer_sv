# Definition of a function to get the rule's required resoruces
def get_resource(rule, resource):
    ''' Get resource info for rule from config file. Return defaults if not found. '''
    try:
        return config["resources"][rule][resource]
    except KeyError:
        return config["resources"]["default"][resource]

# Reference genome indexing
rulename = "minimap2_index_reference"
rule minimap2_index_reference:
    input:
        lambda wildcards: config["genome"][wildcards.version]
    output:
        f"{outdir}/genome/minimap2/{{version}}.mmi"
    conda:
        "../envs/minimap2.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/genome/minimap2/execution/{{version}}.mmi.out",
        err = f"{logdir}/genome/minimap2/execution/{{version}}.mmi.err"
    benchmark:
        f"{logdir}/genome/minimap2/execution/{{version}}.mmi.bmk"
    shell:
        "minimap2 -t {threads} -d {output} {input} > {log.out} 2> {log.err}"
